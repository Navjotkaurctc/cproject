package pageObjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ServicePage {
	WebDriver driver;
	
	//settlement
	@FindBy(xpath="//a[@id='ddlServices-11']")
	WebElement settlement;
		
	//first option in settlement and referrals in services page
	@FindBy(xpath="//input[@value='Citizenship and Immigration']")
	WebElement citizenshipAndImmigration;
		
	//review and proceed button
	@FindBy(xpath="//div[@id='step1-floating-cta']")//button[@id='step1-footer-cta']
	WebElement reviewAndProceedButton;
		
	// dialog review and proceed
	@FindBy(xpath="//button[@class='btn btn-primary btnFindServiceProviders my-1 btn-cta-group']")
	WebElement dialogReviewAndProceed;
	
	public ServicePage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	public ServicePage clickOnSettlmentCouncellingAndReferrals()
	{
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.titleContains("Select Services - Compass to Connect"));
		Assert.assertTrue(driver.getTitle().equals("Select Services - Compass to Connect"));
		settlement.click();
		return this;
	}
	public ServicePage clickOncitizenship()
	{
		if(!citizenshipAndImmigration.isSelected())
		{
			citizenshipAndImmigration.click();			
		}
		return this;
	}
	public ServicePage clickOnReviewAndProceedButton() 
	{
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(reviewAndProceedButton));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", reviewAndProceedButton);
		System.out.println("r and p button has been clicked'");
		
		return this;
	}
	public ServicePage clickToSubmitRequest()
	{
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(dialogReviewAndProceed));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", dialogReviewAndProceed);
		//after check if the control has gone to the next page 
		wait.until(ExpectedConditions.titleContains("Choose an Organization - Compass to Connect"));
		Assert.assertTrue(driver.getTitle().contains("Choose an Organization - Compass to Connect"));
		return this;
	}

}
