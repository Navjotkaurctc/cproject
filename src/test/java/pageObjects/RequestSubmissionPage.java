package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class RequestSubmissionPage {
	WebDriver driver;
	
	@FindBy(xpath="//form[@id='idformemail']//h1[@class='display-2']")
	WebElement thankyouMessage;
	
	@FindBy(xpath="//a[contains(text(),'Logout')]")
	WebElement logoutButton;
	
	public RequestSubmissionPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	public RequestSubmissionPage Message()
	{
		thankyouMessage.getText();
		System.out.println(thankyouMessage.getText());
		Assert.assertTrue(thankyouMessage.getText().contains("Thank You!"));
		return this;
	}
	public RequestSubmissionPage logoutProfile()
	{
		logoutButton.click();
		return this;
	}
}
