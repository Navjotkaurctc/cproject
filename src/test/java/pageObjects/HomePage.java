package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;

@Listeners(listeners.Listeners.class)
public class HomePage {
	WebDriver driver;
	
	@FindBy(xpath="//a[contains(text(),'Sign In')]")
	WebElement signinLink;
	
	
	@FindBy(id="btnusertypeselect")
	WebElement signupButton;
	
	public HomePage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public HomePage clicksignin()
	{
		signinLink.click();
		return this;
	}
	
	public HomePage clicksignup()
	{
		signupButton.click();
		return this;
	}

}
