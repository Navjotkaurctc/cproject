package pageObjects;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import resources.randomDataGeneration;

public class ClientProfile {
	WebDriver driver;
	
	@FindBy(xpath="//textarea[@class='form-control col-12 col-lg-9']")
	WebElement username;
	
	//immigration status
	@FindBy(id="drop_immgcat")
	WebElement immigrationCategory;
	
	//UCI
	@FindBy(id="txtdocref")
	WebElement uci;
	
	//Arrival date
	@FindBy(name="arrivalDate")
	WebElement arrivalDate;
	
	//Landing date
	@FindBy(name="LandingDate")
	WebElement landingDate;
	
	//first name
	@FindBy(xpath="//input[@id='txfname']")
	WebElement firstName;
	
	//last name
	@FindBy(xpath="//input[@id='txtlname']")
	WebElement lastName;
	
	//date of birth
	@FindBy(xpath="//input[@name='DateofBirth' and @placeholder='MM/DD/YYYY']")
	WebElement dob;
	
	//phone number
	@FindBy(id="pnumber")
	WebElement phoneNumber;
	
	//alternative phone number
	@FindBy(id="altpnumber")
	WebElement altNumber;
	
	//street number
	@FindBy(id="StreetNumber")
	WebElement streetNumber;
	
	//street name
	@FindBy(id="StreetName")
	WebElement streetName;
	
	//unit number
	@FindBy(id="UnitNumber")
	WebElement unitNumber;
	
	//province
	@FindBy(id="drop_province")
	WebElement province;
	
	//city
	@FindBy(xpath="//select[@id='drop_provincecity']")
	WebElement city;
	
	//postal code
	@FindBy(id="idpostalcode")
	WebElement postalCode;
	
	//email
	@FindBy(id="idemailadr")
	WebElement email;
	
	//alternative email
	@FindBy(id="idaltemailadr")
	WebElement altEmail;
	
	//country
	@FindBy(id="drop_countrycat")
	WebElement country;
	
	//gender
	@FindBy(id="drop_gendercat")
	WebElement gender;
	
	//marital status
	@FindBy(id="drop_maritalcat")
	WebElement maritalStatus;
	
	//official language
	@FindBy(id="drop_oflangcat")
	WebElement officialLanguage;
	
	//native language
	@FindBy(id="drop_nalangcat")
	WebElement nativeLanguage;
	
	//Educational details
	@FindBy(id="drop_Educationcat")
	WebElement levelOfEducation;
	
	//current enrollment
	@FindBy(id="drop_enrolcat")
	WebElement currentEnrollment;
	
	//continue search button
	@FindBy(xpath ="//form[@id='idformupdateprofile']//button[@class='btn btn-primary btn-cta-group m-2'][contains(text(),'Continue Search')]")
	WebElement continueSearch;
	
	//update profile
	@FindBy(xpath="//button[text()='Update Profile']")
	WebElement updateProfile;
	
	//profile updation confirmation message 
	@FindBy(id="errorMessage")
	WebElement profileUpdationMessage;
	
	//additional information
	@FindBy(id="profile-additional-information-tab")
	WebElement additionalInformation;
	
	//update button for education
	@FindBy(xpath="//div[@id='collapseTwo']//button[@class='btn btn-primary btn-cta-group m-2'][contains(text(),'Update')]")
	WebElement updateEducation;
	
	//employement in canada
	@FindBy(xpath="//a[contains(text(),'Employment in Canada')]")
	WebElement employmentInCanadaAccordian;
	
	//label for source of income this has been created so as to confirm the tab has opened
	@FindBy(xpath="//label[contains(text(),'Source of Income?')]")
	WebElement sourceOfIncomeLabel;
	
	//source of income cat
	@FindBy(id="drop_incomecat")
	WebElement sourceOfIncome;
	
	//employment status 
	@FindBy(id="drop_Employeecat")
	WebElement employmentStatus;
	
	//relevant field
	@FindBy(id="employedRelevantField")
	WebElement employedRelevantField;
	
	//original occupation
	@FindBy(id="autocomplete")
	WebElement originalOccupation;
	
	//original occupation list
	@FindBy(xpath="//ul[@id='ui-id-2']//li")
	WebElement originalOccupationList;
	
	//current occupation
	@FindBy(id="cautocomplete")
	WebElement currentOccupation;
	
	//current occupation list 
	@FindBy(id="ui-id-3")
	WebElement currentOccupationList;
	
	//intended occupation
	@FindBy(id="iautocomplete")
	WebElement intendedOccupation;
	
	//intended occupation list 
	@FindBy(xpath="//ul[@id='ui-id-4']")
	WebElement intendedOccupationList;
	
	//submit button for updating employment in canada details
	@FindBy(xpath="//div[@id='collapseThree']//button[@class='btn btn-primary btn-cta-group m-2'][contains(text(),'Update')]")
	WebElement updateEmployment;
	
	//housing accordian
	@FindBy(xpath="//a[contains(text(),'Housing')]")
	WebElement housingAccordian;
	
	// label in housing acc
	@FindBy(xpath="//label[contains(text(),'What is your current living arrangement?')]")
	WebElement housingLabel;
	
	//living arrangement
	@FindBy(id="drop_livingcat")
	WebElement livingArrangement;
	
	//update housing 
	@FindBy(xpath="//div[@id='collapseOne']//button[@class='btn btn-primary btn-cta-group m-2'][contains(text(),'Update')]")
	WebElement updateHousing;
	
	//Additional family members accordian
	@FindBy(xpath= "//a[contains(text(),'Additional Family Members')]")
	WebElement familyAccordian;
	
	// Add name of family member
	@FindBy(xpath="//input[starts-with(@id,'idName-+var+')]")
	WebElement FullNameFamilyMember;
	
	//continue search button
	@FindBy(xpath="//button[@name='btncontinue']")
	WebElement contSearch;
	
	//wrong error message for wrong uci
	@FindBy(id="txtdocref-error")
	WebElement uciErrorMessage;
	
	//error message of landing date
	@FindBy(id="LandingDate-error")
	WebElement landingErrorMessage;
	
	FileInputStream fis;
	Properties prop;
	
	public ClientProfile(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public String checkUsername()
	{
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(username));
		String user = username.getText();
		username.getAttribute("readonly");
		return user;
	}
	public ClientProfile checkProvinceAndCityCombination()
	{
		Select sc= new Select(province);
		List<WebElement> provinceNames =sc.getOptions();
		for(int i=0;i<=provinceNames.size();i++)
		{
			//System.out.println(provinceNames.get(i).getText());
			List<String> listA = new ArrayList<String>();
			listA.add(provinceNames.get(i).getText());
			System.out.println(listA);
			
		}
		return this;
		//this script is incomplete
	}
	public ClientProfile chooseImmigration(String imm)
	{
		WebDriverWait wait = new WebDriverWait(driver,5);
		try{
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//data.properties");
			prop = new Properties();
			prop.load(fis);
		}catch(FileNotFoundException e )
		{
			e.getStackTrace();
		}catch(IOException e)
		{
			e.getMessage();
		}
		if(prop.getProperty("browser").equals("firefox"))
		{
			
			wait.until(ExpectedConditions.visibilityOf(immigrationCategory));
			WebElement element = driver.findElement(By.id("select2-drop_immgcat-container"));
			element.click();
			WebElement e2= driver.findElement(By.xpath("//input[@class='select2-search__field']"));
			e2.sendKeys(imm);
			e2.sendKeys(Keys.ENTER);
		}
		else
		{
			Select immCategory = new Select(immigrationCategory);
			System.out.println("Select class has been called");
			immCategory.selectByVisibleText(imm);
			System.out.println("imm status got selected ");
		}
		return this;
	}
	public List<WebElement> ListOfImmigrationStatus()
	{
		Select immCategory = new Select(immigrationCategory);
		List<WebElement> cat = immCategory.getOptions();
		return cat;
	}
	public ClientProfile addUCI(String uciNumber)
	{
		uci.clear();
		uci.sendKeys(uciNumber);
		uci.sendKeys(Keys.TAB);
		if(uciNumber.length()<8 || uciNumber.length()>10 || uciNumber.matches("%[a-zA-Z]%")|| uciNumber.matches("[!@#$%&*()_+=|<>?{}\\[\\]~-]"))
		{
			System.out.println("you have entered wrong uci" + uciNumber);
			WebDriverWait wait = new WebDriverWait(driver,2);
			wait.until(ExpectedConditions.visibilityOf(uciErrorMessage));
			System.out.println(uciErrorMessage.getText());
		}
		return this;
	}
	public ClientProfile addArrivalDate(String arrDate)
	{
		arrivalDate.clear();
		arrivalDate.sendKeys(arrDate);
		arrivalDate.sendKeys(Keys.ENTER);
		return this;
	}
	public ClientProfile addLandingDate(String landDate)
	{
		landingDate.clear();
		landingDate.sendKeys(landDate);
		landingDate.sendKeys(Keys.ENTER);
		return this;
	}
	public ClientProfile addDob(String dateofBirth)
	{
		dob.clear();
		dob.sendKeys(dateofBirth);
		if(prop.getProperty("browser").equals("firefox"))
		{
			dob.sendKeys(Keys.ENTER);
		}
		return this;
	}
	public ClientProfile addFirstName(String fname)
	{
		firstName.clear();
		firstName.sendKeys(fname);
		return this;
	}
	public ClientProfile addLastName(String lname)
	{
		lastName.clear();
		lastName.sendKeys(lname);
		return this;
	}
	public ClientProfile addPrimaryPhoneNumber(String phone)
	{
		System.out.println(phoneNumber.getText());
		phoneNumber.clear();
		phoneNumber.sendKeys(phone);
		System.out.println(phoneNumber.getText());
		return this;
	}
	public ClientProfile addAlternativePhoneNumber(String phone)
	{
		altNumber.clear();
		altNumber.sendKeys(phone);
		return this;
	}
	public ClientProfile addStreetNumber(String street)
	{
		streetNumber.clear();
		streetNumber.sendKeys(street);
		return this;
	}
	public ClientProfile addStreetName(String stname)
	{
		streetName.clear();
		streetName.sendKeys(stname);
		return this;
	}
	public ClientProfile addUnitNumber(String unit)
	{
		unitNumber.clear();
		unitNumber.sendKeys(unit);
		if(unit.length()<=1)
		{
			System.out.println("User has not entered any value in unit number field, but it is acceptable as it is required field");
			unitNumber.sendKeys(Keys.TAB);
			
		}
		return this;
	}
	public ClientProfile selectProvince(String provinceValue)
	{
		if(prop.getProperty("browser").equals("firefox"))
		{
			WebElement element = driver.findElement(By.id("select2-drop_province-container"));
			try {
				Thread.sleep(2000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("window.scrollBy(0,2000)");
			try {
				Thread.sleep(3000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			element.click();
			WebElement e2 = driver.findElement(By.xpath("//input[@class='select2-search__field']"));
			e2.sendKeys(provinceValue);
			driver.findElement(By.xpath("//input[@class='select2-search__field']")).sendKeys(Keys.ENTER);
		}
		else
		{
			Select prov = new Select(province);
			prov.selectByVisibleText(provinceValue);
		}
		
		return this;
	}
	public ClientProfile checkProvinceOptions()
	{
		Select prov = new Select(province);
		List<WebElement> list = prov.getOptions();
		int j=0;
		for(int i=0;i<list.size();i++)
		{
			if(randomDataGeneration.provinceList().contains(list.get(i).getText()))
			{
				j++;
			}
			else
			{
				System.out.println("Province list doesn't matches with the provided list ");
			}
			
		}
		if(j==14)
		{
			System.out.println("Province list is valid according to radom data generation list");
		}
		return this;
	}
	public ClientProfile selectCity(String provinceValue, String cityValue )
	{
		
		if(provinceValue.equalsIgnoreCase("Ontario (ON)"))
		{
			if(randomDataGeneration.OntarioCities().contains(cityValue))
			{
				System.out.println("the city that you have selected falls under Ontario which is correct");
			}
			else
			{
				System.out.println("the city that you have selected falls under some other province");
			}
		}
		else if(provinceValue.equalsIgnoreCase("Alberta (AB)"))
		{
			if(randomDataGeneration.AlbertaCities().contains(cityValue))
			{
				System.out.println("the city that you have selected falls under Alberta which is correct");
			}
			else
			{
				System.out.println("the city that you have selected falls under some other province");
			}
		}
		else if(provinceValue.equalsIgnoreCase("British Columbia (BC)"))
		{
			if(randomDataGeneration.BritishColumbia().contains(cityValue))
			{
				System.out.println("the city that you have selected falls under British Columbia which is correct");
			}
			else
			{
				System.out.println("the city that you have selected falls under some other province");
			}
		}
		else if(provinceValue.equalsIgnoreCase("Manitoba (MB)"))
		{
			if(randomDataGeneration.Manitoba().contains(cityValue))
			{
				System.out.println("the city that you have selected falls under Manitoba which is correct");
			}
			else
			{
				System.out.println("the city that you have selected falls under some other province");
			}
		}
		else if(provinceValue.equalsIgnoreCase("New Brunswick (NB)"))
		{
			if(randomDataGeneration.NewBrunswick().contains(cityValue))
			{
				System.out.println("the city that you have selected falls under New Brunswick which is correct");
			}
			else
			{
				System.out.println("the city that you have selected falls under some other province");
			}
		}
		else if(provinceValue.equalsIgnoreCase("Newfoundland and Labrador (NL)"))
		{
			if(randomDataGeneration.Newfoundland().contains(cityValue))
			{
				System.out.println("the city that you have selected falls under Newfoundland and Labrador which is correct");
			}
			else
			{
				System.out.println("the city that you have selected falls under some other province");
			}
		}
		else if(provinceValue.equalsIgnoreCase("Northwest Territories (NT)"))
		{
			if(randomDataGeneration.NorthwestTerritories().contains(cityValue))
			{
				System.out.println("the city that you have selected falls under Northwest Territories which is correct");
			}
			else
			{
				System.out.println("the city that you have selected falls under some other province");
			}
		}
		else if(provinceValue.equalsIgnoreCase("Nova Scotia (NS)"))
		{
			if(randomDataGeneration.NovaScotia().contains(cityValue))
			{
				System.out.println("the city that you have selected falls under Nova Scotia which is correct");
			}
			else
			{
				System.out.println("the city that you have selected falls under some other province");
			}
		}
		else if(provinceValue.equalsIgnoreCase("Nunavut (NU)"))
		{
			if(randomDataGeneration.Nunavut().contains(cityValue))
			{
				System.out.println("the city that you have selected falls under Nunavut which is correct");
			}
			else
			{
				System.out.println("the city that you have selected falls under some other province");
			}
		}
		else if(provinceValue.equalsIgnoreCase("Prince Edward Island (PE)"))
		{
			if(randomDataGeneration.PrinceEdwardIsland().contains(cityValue))
			{
				System.out.println("the city that you have selected falls under Prince Edward Island which is correct");
			}
			else
			{
				System.out.println("the city that you have selected falls under some other province");
			}
		}
		else if(provinceValue.equalsIgnoreCase("Quebec (QC)"))
		{
			if(randomDataGeneration.Quebec().contains(cityValue))
			{
				System.out.println("the city that you have selected falls under Quebec which is correct");
			}
			else
			{
				System.out.println("the city that you have selected falls under some other province");
			}
		}
		else if(provinceValue.equalsIgnoreCase("Saskatchewan (SK)"))
		{
			if(randomDataGeneration.Saskatchewan().contains(cityValue))
			{
				System.out.println("the city that you have selected falls under Saskatchewan which is correct");
			}
			else
			{
				System.out.println("the city that you have selected falls under some other province");
			}
		}
		else if(provinceValue.equalsIgnoreCase("Yukon (YT)"))
		{
			if(randomDataGeneration.Yukon().contains(cityValue))
			{
				System.out.println("the city that you have selected falls under Yukon which is correct");
			}
			else
			{
				System.out.println("the city that you have selected falls under some other province");
			}
		}
		if(prop.getProperty("browser").equals("firefox"))
		{
			WebElement element = driver.findElement(By.id("select2-drop_provincecity-container"));
			try {
				Thread.sleep(5000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			element.click();
			WebElement e2= driver.findElement(By.xpath("//input[@class='select2-search__field']"));
			e2.sendKeys(cityValue);
			e2.sendKeys(Keys.ENTER);
		}
		else
		{
			Select cityselectbox = new Select(city);
			cityselectbox.selectByVisibleText(cityValue);
			
		}
		return this;
	}
	
	public ClientProfile addPostalCode(String postal)
	{
		postalCode.clear();
		String pocode = postal.toUpperCase();
		postalCode.sendKeys(pocode);
	
		String regex = "^(?!.*[DFIOQU])[A-VXY][0-9][A-Z]●?[0-9][A-Z][0-9]$";
		Pattern p = Pattern.compile(regex);
		if(pocode.matches(regex) && pocode.length()==6)
		{
			Matcher matcher = p.matcher(pocode);
		    System.out.println(matcher.matches());
		}
		else
		{
			System.out.println("The user is adding wrong format of postal code");
		}
		return this;
	}
	public String checkPrimaryEmail()
	{
		String user = email.getText();
		email.getAttribute("readonly");
		return user;
	}
	public ClientProfile checkOtherEmail(String alt)
	{
		altEmail.getText();
		if(altEmail.getText().length()==0)
		{
			System.out.println("Since alternative field was already empty and it is an optional field move ahead with further steps");
		}
		else if(altEmail.getText()!=alt)
		{
			System.out.println("Alternative email is diffrent from primary email so it is accepted");
			altEmail.clear();
			altEmail.sendKeys(alt);
			altEmail.sendKeys(Keys.TAB);
		}
		else if(altEmail.getText().equals(email.getText()))
		{
			altEmail.clear();
			System.out.println("Since alternative email cannot be same as primary email that is why script is clearing the field");
		}
				
		
		return this;
	}
	public ClientProfile chooseCountryOfOrigin(String origin)
	{
		WebDriverWait wait = new WebDriverWait(driver,5);
		if(prop.getProperty("browser").equals("firefox"))
		{
			wait.until(ExpectedConditions.visibilityOf(country));
			WebElement element = driver.findElement(By.id("select2-drop_countrycat-container"));
			element.click();
			try {
				Thread.sleep(5000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement e2=driver.findElement(By.xpath("//input[@class='select2-search__field']"));
			e2.sendKeys(origin);
			e2.sendKeys(Keys.ENTER);
			System.out.println("Country of origin has been selected");
		}
		Select countryoforigin = new Select(country);
		countryoforigin.selectByVisibleText(origin);
		return this;
	}
	public List<WebElement> ListOfCountryOfOrigin()
	{
		Select list = new Select(country);
		List<WebElement> li = list.getOptions();
		return li;
	}
	public ClientProfile chooseNativeLanguage(String nativelanguage)
	{
		if(prop.getProperty("browser").equals("firefox"))
		{
			WebElement element = driver.findElement(By.id("select2-drop_nalangcat-container"));
			element.click();
			try {
				Thread.sleep(3000L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			WebElement e2=driver.findElement(By.xpath("//input[@class='select2-search__field']"));
			e2.sendKeys(nativelanguage);
			e2.sendKeys(Keys.ENTER);
			System.out.println("native lanaguage has been selected");
		}
		else
		{
			Select nativelang = new Select(nativeLanguage);
			nativelang.selectByVisibleText(nativelanguage);
		}
		
		return this;
	}
	public ClientProfile chooseCanadianOfficialLanguage(String offlang)
	{
		if(prop.getProperty("browser").equals("firefox"))
		{
			WebElement element = driver.findElement(By.id("select2-drop_oflangcat-container"));
			element.click();
			try {
				Thread.sleep(3000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement e2=driver.findElement(By.xpath("//input[@class='select2-search__field']"));
			e2.sendKeys(offlang);
			e2.sendKeys(Keys.ENTER);
			System.out.println("official lanaguage has been selected");
		}
		else
		{
			Select canadianLang = new Select(officialLanguage);
			canadianLang.selectByVisibleText(offlang);
		}
		return this;
	}
	public ClientProfile chooseGender(String gen)
	{
		if(prop.getProperty("browser").equals("firefox"))
		{
			WebElement element = driver.findElement(By.id("select2-drop_gendercat-container"));
			element.click();
			try {
				Thread.sleep(3000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement e2=driver.findElement(By.xpath("//input[@class='select2-search__field']"));
			e2.sendKeys(gen);
			e2.sendKeys(Keys.ENTER);
		}
		else
		{
			Select gend = new Select(gender);
			gend.selectByVisibleText(gen);
		}
		
		return this;
	}
	public ClientProfile chooseMaritalStatus(String mar)
	{
		if(prop.getProperty("browser").equals("firefox"))
		{
			WebElement element = driver.findElement(By.id("select2-drop_maritalcat-container"));
			element.click();
			try {
				Thread.sleep(3000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				WebElement e2=driver.findElement(By.xpath("//input[@class='select2-search__field']"));
				e2.sendKeys(mar);
				e2.sendKeys(Keys.ENTER);
			}
		}
		else
		{
			Select marital = new Select(maritalStatus);
			marital.selectByVisibleText(mar);
		}
		System.out.println("all info updated");
		return this;
	}
	public ClientProfile clickUpdateProfile()
	{
		WebDriverWait wait = new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.elementToBeClickable(updateProfile));
		updateProfile.submit();
		System.out.println("update profile got clicked");
		return this;
		//please use submit() not click() for forms as click() won't work and update the values to get the message as submit()
	}
	public ClientProfile checkMessage()
	{
		WebDriverWait wait = new WebDriverWait(driver,25);
		wait.until(ExpectedConditions.visibilityOf(profileUpdationMessage));
		System.out.println(profileUpdationMessage.getText());
		return this;
	}
	public ClientProfile clickAdditionalInformationTab()
	{
		for(int i=0;i<=3;i++)
		{
			try
			{
				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("arguments[0].scrollIntoView(true);", additionalInformation);
				Thread.sleep(2000L);
				additionalInformation.click();
				break;
			}catch(Exception ex)
			{
				System.out.println("stale element reference");
				ex.printStackTrace();
			}
		}
		
		return this;
	}
	public ClientProfile chooseLevelOfEducation(String edu)
	{
		WebDriverWait wait = new WebDriverWait(driver,5);
		wait.until(ExpectedConditions.visibilityOf(levelOfEducation));
		//levelOfEducation.click();
		if(prop.getProperty("browser").equals("firefox"))
		{
			WebElement element = driver.findElement(By.id("select2-drop_Educationcat-container"));
			element.click();
			try {
				Thread.sleep(2000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement e2=driver.findElement(By.xpath("//input[@class='select2-search__field']"));
			e2.sendKeys(edu);
			e2.sendKeys(Keys.ENTER);
		}
		else
		{
			Select education = new Select(levelOfEducation);
			education.selectByVisibleText(edu);
		}
		return this;
	}
	public ClientProfile chooseCurrentEnrollment(String enroll)
	{
		WebDriverWait wait = new WebDriverWait(driver,5);
		wait.until(ExpectedConditions.elementToBeClickable(currentEnrollment));
		if(prop.getProperty("browser").equals("firefox"))
		{
			WebElement element = driver.findElement(By.id("select2-drop_enrolcat-container"));
			element.click();
			try {
				Thread.sleep(2000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement e2=driver.findElement(By.xpath("//input[@class='select2-search__field']"));
			e2.sendKeys(enroll);
			e2.sendKeys(Keys.ENTER);
		}
		else
		{
			Select currentenroll = new Select(currentEnrollment);
			currentenroll.selectByVisibleText(enroll);
		}
		return this;
	}
	public ClientProfile submitEducationalDetails()
	{
		updateEducation.submit();
		System.out.println("educational details has been submitted ");
		return this;
	}
	public ClientProfile clickEmploymentInCanada()
	{
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView(true);", employmentInCanadaAccordian);
		WebDriverWait wait = new WebDriverWait(driver,5);
		wait.until(ExpectedConditions.visibilityOf(employmentInCanadaAccordian));
		employmentInCanadaAccordian.click();
		
		wait.until(ExpectedConditions.visibilityOf(sourceOfIncomeLabel));
		System.out.println("Employment in Canada Accordian has been opened succefully ");
		if(!sourceOfIncomeLabel.isDisplayed())
		{
			employmentInCanadaAccordian.click();
			System.out.println("employment in canada was clicked twice to show all the options please check the script or the application for errors");
		}
		return this;
	}
	public ClientProfile chooseSourceOfIncome(String source)
	{
		if(prop.getProperty("browser").equals("firfox"))
		{
			WebElement element = driver.findElement(By.id("select2-drop_incomecat-container"));
			element.click();
			try {
				Thread.sleep(1000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement e2 = driver.findElement(By.xpath("//input[@class='select2-search__field']"));
			e2.sendKeys(source);
			e2.sendKeys(Keys.ENTER);
		}
		else
		{
			Select s = new Select(sourceOfIncome);
			s.selectByVisibleText(source);
		}
		return this;
	}
	public ClientProfile chooseEmploymentStatus(String empStatus)
	{
		if(prop.getProperty("browser").equals("firefox"))
		{
			WebElement element = driver.findElement(By.id("select2-drop_Employeecat-container"));
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].scrollIntoView(true);", element);
			try {
				Thread.sleep(5000L);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			element.click();
			try {
				Thread.sleep(1000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement e2 = driver.findElement(By.xpath("//input[@class='select2-search__field']"));
			e2.sendKeys(empStatus);
			e2.sendKeys(Keys.ENTER);
		}
		else
		{
			Select s = new Select(employmentStatus);
			s.selectByVisibleText(empStatus);
		}
		return this;
	}
	public ClientProfile chooseRelevantFieldOption(String relevant)
	{
		if(prop.getProperty("browser").equals("firefox"))
		{
			WebElement element = driver.findElement(By.id("select2-employedRelevantField-container"));
			element.click();
			try {
				Thread.sleep(1000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement e2 = driver.findElement(By.xpath("//input[@class='select2-search__field']"));
			e2.sendKeys(relevant);
			e2.sendKeys(Keys.ENTER);
		}
		else
		{
			Select s = new Select(employedRelevantField);
			s.selectByVisibleText(relevant);
		}
		return this;
	}
	public ClientProfile chooseOriginalOccupation(String occ)
	{
		originalOccupation.clear();
		originalOccupation.sendKeys(occ);
		WebDriverWait wait = new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.visibilityOf(originalOccupationList));
		originalOccupation.sendKeys(Keys.ARROW_DOWN);
		Assert.assertTrue(originalOccupationList.isDisplayed());
		System.out.println("the list is displayed with respect to the text provided");
		List<WebElement> li = driver.findElements(By.xpath("//ul[@id='ui-id-2']//li"));
		System.out.println(li.size());
		originalOccupation.sendKeys(Keys.ENTER);
		return this;
	}
	public ClientProfile chooseCurrentOcccupation(String current)
	{
		currentOccupation.clear();
		currentOccupation.sendKeys(current);
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(currentOccupationList));
		currentOccupation.sendKeys(Keys.ARROW_DOWN);
		Assert.assertTrue(currentOccupationList.isDisplayed());
		System.out.println("the list has been displayed with respect to the text provided");
		List<WebElement> li = driver.findElements(By.xpath("//ul[@id='ui-id-2']//li"));
		System.out.println(li.size());
		currentOccupation.sendKeys(Keys.ENTER);
		return this;	
	}
	public ClientProfile chooseIntendedOccupation(String intended)
	{
		intendedOccupation.clear();
		intendedOccupation.sendKeys(intended);
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(intendedOccupationList));
		intendedOccupation.sendKeys(Keys.ARROW_DOWN);
		Assert.assertTrue(intendedOccupationList.isDisplayed());
		System.out.println("the list has been displayed with respect to the text provided");
		List<WebElement> li = driver.findElements(By.xpath("//ul[@id='ui-id-3']//li"));
		System.out.println(li.size());
		intendedOccupation.sendKeys(Keys.ENTER);
		return this;
	}
	public ClientProfile clickUpdateEmployment()
	{
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(updateEmployment));
		updateEmployment.submit();
		return this;
	}
	public ClientProfile clickHousingAccordian() 
	{
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView(true);", housingAccordian);
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(housingAccordian));
		housingAccordian.click();
		wait.until(ExpectedConditions.visibilityOf(housingLabel));
		Assert.assertTrue(housingLabel.isDisplayed());
		return this;
	}
	public ClientProfile chooseAccomodation(String accomodation)
	{
		if(prop.getProperty("browser").equals("firefox"))
		{
			WebElement element = driver.findElement(By.id("select2-drop_livingcat-container"));
			WebDriverWait wait = new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.elementToBeClickable(element));
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].scrollIntoView(true);", element);
			element.click();
			try {
				Thread.sleep(1000L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			WebElement e2 = driver.findElement(By.xpath("//input[@class='select2-search__field']"));
			e2.sendKeys(accomodation);
			e2.sendKeys(Keys.ENTER);
		}
		else
		{
			Select s = new Select(livingArrangement);
			s.selectByVisibleText(accomodation);
		}
		return this;
	}
	public ClientProfile updateHousingDetails()
	{
		WebDriverWait wait = new WebDriverWait(driver,5);
		wait.until(ExpectedConditions.elementToBeClickable(updateHousing));
		updateHousing.click();
		return this;
	}
	public ClientProfile clickFamilyAccordian()
	{
		familyAccordian.click();
		return this;
	}
	public ClientProfile addFamilyMemberDetails(String fullname, String relationship, String uci, String dob)
	{
		
		for(int i=0;i<=10;i++)
		{
			WebElement element = driver.findElement(By.xpath("//input[@id='idName-"+i+"']"));
			String textInsideInputBox = element.getAttribute("value");
			if(textInsideInputBox.isEmpty())
			{
				element.sendKeys(fullname);
				WebElement relationshipCategory = driver.findElement(By.xpath("//select[@id='drop_citycat-"+i+"']"));
				if(prop.getProperty("browser").equals("firefox"))
				{
					WebElement rel = driver.findElement(By.id("select2-drop_citycat-"+i+"-container"));
					rel.click();
					try {
						Thread.sleep(1000L);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					WebElement relInput = driver.findElement(By.xpath("//input[@class='select2-search__field']"));
					relInput.sendKeys(relationship);
					relInput.sendKeys(Keys.ENTER);
				}
				else
				{
					Select s = new Select(relationshipCategory);
					s.selectByVisibleText(relationship);
				}
				
				WebElement uciFamily = driver.findElement(By.xpath("//input[@id='idDocumentNumber-"+i+"']"));
				uciFamily.sendKeys(uci);
				//WebElement dob = driver.findElement(By.xpath("//input[@id='idDateofBirth-"+i+"']"));
				JavascriptExecutor js=(JavascriptExecutor)driver;
				WebElement inputDate = driver.findElement(By.xpath("//input[@id='idDateofBirth-"+i+"']"));
				js.executeScript("arguments[0].setAttribute('value', '"+dob+"')", inputDate);
				inputDate.sendKeys(Keys.ENTER);
				WebElement addMember = driver.findElement(By.xpath("//button[@id='idbtnadd-"+i+"']"));
				js.executeScript("arguments[0].scrollIntoView(true);", addMember);
				addMember.click();
				//ensure that your family member has been added by seeing if update button is visibleidbtnupdate-0//idbtnupdate-0
				WebElement updateButton = driver.findElement(By.xpath("//button[@id='idbtnupdate-"+i+"']"));
				WebDriverWait wait = new WebDriverWait(driver,10);
				wait.until(ExpectedConditions.visibilityOf(updateButton));
				Assert.assertTrue(updateButton.isDisplayed());
				System.out.println("your family member has been successfully added");
				contSearch.click();
				break;
			}
		}
		return this;
	}
	
}
