package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WelcomeUserPage {
	WebDriver driver;
	
	@FindBy(id="dropdownMenuLink")
	WebElement user;
	
	@FindBy(xpath="//a[contains(text(),'Profile')]")
	WebElement userProfile;
	
	@FindBy(xpath="//a[@href='#profile-settings-preference']")
	WebElement settingsAndPreference;
	
	@FindBy(xpath="//form[@action='/Profile/UpdateCredential']")
	WebElement changePasswordAccordian;
	
	@FindBy(id="currentpass")
	WebElement currentPassword;
	
	@FindBy(id="newpass")
	WebElement newPassword;
	
	@FindBy(id="newpassRepeat")
	WebElement confirmNewPassword;
	
	@FindBy(xpath="//button[text()='Save Password']")
	WebElement savePassword;
	
	@FindBy(id="passwordErrorMessage")
	WebElement passwordErrorMessage;
	
	@FindBy(id="errorMessage")
	WebElement passwordSuccessfullyChanged;
	
	public WelcomeUserPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public WelcomeUserPage clickUser()
	{
		user.click();
		return this;
	}
	
	public WelcomeUserPage clickProfile()
	{
		userProfile.click();
		return this;
	}
	public WelcomeUserPage clickSettingsAndPreference()
	{
		settingsAndPreference.click();
		return this;
	}
	public WelcomeUserPage clickChangePasswordAccordian()
	{
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(changePasswordAccordian));
		changePasswordAccordian.click();
		return this;
	}
	public WelcomeUserPage chanePasswordWithValidations(String currentPass, String newPass, String confirmNewPass)
	{
		WebDriverWait wait = new WebDriverWait(driver,5);
		wait.until(ExpectedConditions.visibilityOf(currentPassword));
		currentPassword.sendKeys(currentPass);
		newPassword.sendKeys(newPass);
		confirmNewPassword.sendKeys(confirmNewPass);
		savePassword.click();
		try
		{
			if(passwordSuccessfullyChanged.isDisplayed())
			{
				System.out.println(passwordSuccessfullyChanged.getText());
			}
			else
			{
				wait.until(ExpectedConditions.visibilityOf(passwordErrorMessage));
				if(passwordErrorMessage.isDisplayed())
				{
					System.out.println("Password dis not successfully changed");
					System.out.println(passwordErrorMessage.getText());
				}
				else
				{
					System.out.println("Error message is not displayed");
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			
		}
		return this;
	}
}
