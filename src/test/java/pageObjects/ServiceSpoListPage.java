package pageObjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ServiceSpoListPage {
	WebDriver driver;
	
	//this is the first spo that dispalys on the listing according to the search 
	@FindBy(xpath = "//div[@class='organization-cta col-12 col-lg-3']//button[1]")
	WebElement firstSpo;
	
	//select inside the spo 
	@FindBy(id="idbtnselect")
	WebElement selectSpo;
	
	//submit my request button
	@FindBy(xpath="//button[@class='btn btn-primary btnSubmitMyRequest btn-cta-group']")
	WebElement submitRequest;
	
	//view more services
	@FindBy(xpath="//button[@class='btn btn-secondary btn-cta-group']")
	WebElement viewMoreServices;
	
	//color of first attribute
	@FindBy(xpath="//div[@class='card-header' and @id='idorgname-2']")
	WebElement fSpo;
	
	//tab
	@FindBy(xpath="//div[@id='step2-floating-cta']")
	WebElement submitButtonTab;
	
	public ServiceSpoListPage(WebDriver driver)
	{
		this.driver= driver;
		PageFactory.initElements(driver, this);
	}
	public ServiceSpoListPage clickOnViewAndSelectService()
	{
		firstSpo.click();
		return this;
	}
	public ServiceSpoListPage clickOnSelectedSpo()
	{
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", selectSpo);
		return this;
	}
	public ServiceSpoListPage submitRequest()
	{
		try{
			submitRequest.click();
		}
		catch(Exception ex)
		{
			System.out.println("stale element refrence");
		    System.out.println(fSpo.getAttribute("style"));
			submitButtonTab.click();
			try {
				Thread.sleep(5000L);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//WebDriverWait wait = new WebDriverWait(driver,10);
				//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[@class='btn btn-primary btnSubmitMyRequest btn-cta-group']"))));
				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("arguments[0].scrollIntoView(true);", submitRequest);
				js.executeScript("arguments[0].click();", submitRequest);
				System.out.println("submit button got clicked ");
		}
		return this;
	}

}
