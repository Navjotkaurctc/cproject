package pageObjects;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;

@Listeners(listeners.Listeners.class)
public class SigninPage {
	
	WebDriver driver;
	
	@FindBy(xpath="//input[@id='txtemailadr']")
	WebElement email;
	
	@FindBy(xpath="//input[@id='txtpass']")
	WebElement pass;
	
	@FindBy(xpath="//button[@class='btn btn-primary btn-cta-lg']")
	WebElement signinButton;
	
	public SigninPage(WebDriver driver)
	{
		this.driver= driver;
		PageFactory.initElements(driver, this);
	}
	
	public String addEmail(String username)
	{
		email.sendKeys(username);
		return username;
	}
	public String addPassword(String password)
	{
		pass.sendKeys(password);
		return password;
	}
	public WebElement signin()
	{
		signinButton.click();
		return signinButton;
	}

	
}
