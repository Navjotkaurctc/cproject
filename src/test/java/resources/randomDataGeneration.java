package resources;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class randomDataGeneration {
	
	public static String uci8()
	{
		String generatedString = RandomStringUtils.randomNumeric(8);
		return generatedString;
	}
	public static String uci10()
	{
		String generatedString = RandomStringUtils.randomNumeric(10);
		return generatedString;
	}
	public static String phone()
	{
		String generatedString = RandomStringUtils.randomNumeric(10);
		return generatedString;
	}
	public static String randomName()
	{
		String generatedString = RandomStringUtils.randomAlphabetic(6);
		return generatedString;
	}
	public static void immList()
	{
		List<String> immStatus = new ArrayList<String>();
		immStatus.add("Select immigration status");
		immStatus.add("Permanent Resident of Canada");
		immStatus.add("Protected Person");
		immStatus.add("Pending Verification (Received Letter of Approval)");
		immStatus.add("Convention Refugees");
		immStatus.add("Live-in Caregivers");
	}
	public static String chooseRandomImmigrationOption(List<String> immStatus) 
    { 
        Random rand = new Random(); 
        return immStatus.get(rand.nextInt(immStatus.size())); 
    }
	public static List<String> provinceList()
	{
		List<String> province = new ArrayList<String>();
		province.add("Select your province");
		province.add("Ontario (ON)");
		province.add("Alberta (AB)");
		province.add("British Columbia (BC)");
		province.add("Manitoba (MB)");
		province.add("New Brunswick (NB)");
		province.add("Newfoundland and Labrador (NL)");
		province.add("Northwest Territories (NT)");
		province.add("Nova Scotia (NS)");
		province.add("Nunavut (NU)");
		province.add("Prince Edward Island (PE)");
		province.add("Quebec (QC)");
		province.add("Saskatchewan (SK)");
		province.add("Yukon (YT)");
		return province;
	}
	public static String chooseRandomProvince(List<String> province)
	{
		Random rand = new Random();
		return province.get(rand.nextInt(province.size()));
	}
	
	public static List<String> OntarioCities()
	{
		List<String> Ontario= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}
				while(rows.hasNext())
				{
					Row r = rows.next();
				
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Ontario"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Ontario.add(cv.next().getStringCellValue());
						}
					}
				
				}
			}
			
			System.out.println(Ontario);
			
		}
		return Ontario;
	}
	public static List<String> AlbertaCities()
	{
		List<String> Alberta= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}				
				while(rows.hasNext())
				{
					Row r = rows.next();
				
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Alberta"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Alberta.add(cv.next().getStringCellValue());
						}
					}
				
				}
			}
			
			System.out.println(Alberta);
			
		}
		return Alberta;
	}
	
	public static List<String> BritishColumbia()
	{
		List<String> BritishColumbia= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}				
				while(rows.hasNext())
				{
					Row r = rows.next();
				
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("British Columbia"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							BritishColumbia.add(cv.next().getStringCellValue());
						}
					}
				
				}
			}
			
			System.out.println(BritishColumbia);
			
		}
		return BritishColumbia;
	}
	public static List<String> Manitoba()
	{
		List<String> Manitoba= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}				
				while(rows.hasNext())
				{
					Row r = rows.next();
				
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Manitoba"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Manitoba.add(cv.next().getStringCellValue());
						}
					}
				
				}
			}
			
			System.out.println(Manitoba);
			
		}
		return Manitoba;
	}
	public static List<String> NewBrunswick()
	{
		List<String> NewBrunswick= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}				
				while(rows.hasNext())
				{
					Row r = rows.next();
				
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("New Brunswick"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							NewBrunswick.add(cv.next().getStringCellValue());
						}
					}
				
				}
			}
			
			System.out.println(NewBrunswick);
			
		}
		return NewBrunswick;
	}
	public static List<String> Newfoundland()
	{
		List<String> Newfoundland= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}				
				while(rows.hasNext())
				{
					Row r = rows.next();
				
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Newfoundland and Labrador"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Newfoundland.add(cv.next().getStringCellValue());
						}
					}
				
				}
			}
			
			System.out.println(Newfoundland);
			
		}
		return Newfoundland;
	}
	public static List<String> NorthwestTerritories()
	{
		List<String> NorthwestTerritories= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}				
				while(rows.hasNext())
				{
					Row r = rows.next();
				
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Northwest Territories"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							NorthwestTerritories.add(cv.next().getStringCellValue());
						}
					}
				
				}
			}
			
			System.out.println(NorthwestTerritories);
			
		}
		return NorthwestTerritories;
	}
	public static List<String> NovaScotia()
	{
		List<String> NovaScotia= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}				
				while(rows.hasNext())
				{
					Row r = rows.next();
				
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Nova Scotia"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							NovaScotia.add(cv.next().getStringCellValue());
						}
					}
				
				}
			}
			
			System.out.println(NovaScotia);
			
		}
		return NovaScotia;
	}
	public static List<String> Nunavut()
	{
		List<String> Nunavut= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}				
				while(rows.hasNext())
				{
					Row r = rows.next();
				
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Nunavut"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Nunavut.add(cv.next().getStringCellValue());
						}
					}
				
				}
			}
			
			System.out.println(Nunavut);
			
		}
		return Nunavut;
	}
	public static List<String> PrinceEdwardIsland()
	{
		List<String> PrinceEdwardIsland= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}				
				while(rows.hasNext())
				{
					Row r = rows.next();
				
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Prince Edward Island"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							PrinceEdwardIsland.add(cv.next().getStringCellValue());
						}
					}
				
				}
			}
			
			System.out.println(PrinceEdwardIsland);
			
		}
		return PrinceEdwardIsland;
	}
	public static List<String> Quebec()
	{
		List<String> Quebec= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}				
				while(rows.hasNext())
				{
					Row r = rows.next();
				
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Quebec"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Quebec.add(cv.next().getStringCellValue());
						}
					}
				
				}
			}
			
			System.out.println(Quebec);
			
		}
		return Quebec;
	}
	public static List<String> Saskatchewan()
	{
		List<String> Saskatchewan= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}				
				while(rows.hasNext())
				{
					Row r = rows.next();
				
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Saskatchewan"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Saskatchewan.add(cv.next().getStringCellValue());
						}
					}
				
				}
			}
			
			System.out.println(Saskatchewan);
			
		}
		return Saskatchewan;
	}
	public static List<String> Yukon()
	{
		List<String> Yukon= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}				
				while(rows.hasNext())
				{
					Row r = rows.next();
				
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Yukon"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Yukon.add(cv.next().getStringCellValue());
						}
					}
				
				}
			}
			
			System.out.println(Yukon);
			
		}
		return Yukon;
	}
	public static void cityListForOntario()
	{
		List<String> Alberta = new ArrayList<String>();
		List<String> BritishColumbia= new ArrayList<String>();
		List<String> Manitoba= new ArrayList<String>();
		List<String> NewBrunswick= new ArrayList<String>();
		List<String> NewfoundlandandLabrador = new ArrayList<String>();
		List<String> NorthwestTerritories=new ArrayList<String>();
		List<String> NovaScotia= new ArrayList<String>();
		List<String> Nunavut=new ArrayList<String>();
		List<String> Ontario= new ArrayList<String>();
		List<String> PrinceEdwardIsland= new ArrayList<String>();
		List<String> Quebec= new ArrayList<String>();
		List<String> Saskatchewan= new ArrayList<String>();
		List<String> Yukon= new ArrayList<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//all canada cities.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("Sheet1"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row firstrow= rows.next();
				Iterator<Cell> cell=firstrow.cellIterator();
				int k=0;int column = 0;
				while(cell.hasNext())
				{
					Cell value = cell.next();
					if(value.getStringCellValue().equalsIgnoreCase("ProvinceName"))
					{
						column=k;
						Cell val = cell.next();
						val.getStringCellValue();
					}
					k++;
				}
				System.out.println(column);
				
				while(rows.hasNext())
				{
					Row r = rows.next();
					if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Alberta"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Alberta.add(cv.next().getStringCellValue());
						}
					}
					else if(r.getCell(column).getStringCellValue().equalsIgnoreCase("British Columbia"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							BritishColumbia.add(cv.next().getStringCellValue());
						}
					}
					else if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Manitoba"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Manitoba.add(cv.next().getStringCellValue());
						}
					}
					else if(r.getCell(column).getStringCellValue().equalsIgnoreCase("New Brunswick"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							NewBrunswick.add(cv.next().getStringCellValue());
						}
					}
					else if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Newfoundland and Labrador"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							NewfoundlandandLabrador.add(cv.next().getStringCellValue());
						}
					}
					else if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Northwest Territories"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							NorthwestTerritories.add(cv.next().getStringCellValue());
						}
					}
					else if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Nova Scotia"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							NovaScotia.add(cv.next().getStringCellValue());
						}
					}
					else if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Nunavut"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Nunavut.add(cv.next().getStringCellValue());
						}
					}
					else if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Ontario"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Ontario.add(cv.next().getStringCellValue());
						}
					}
					else if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Prince Edward Island"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							PrinceEdwardIsland.add(cv.next().getStringCellValue());
						}
					}
					else if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Quebec"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Quebec.add(cv.next().getStringCellValue());
						}
					}
					
					else if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Saskatchewan"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Saskatchewan.add(cv.next().getStringCellValue());
						}
					}
					else if(r.getCell(column).getStringCellValue().equalsIgnoreCase("Yukon"))
					{
						Iterator<Cell>cv= r.cellIterator();
						while(cv.hasNext())
						{
							cv.next().getStringCellValue();
							Yukon.add(cv.next().getStringCellValue());
						}
					}
					
				}
			}
			System.out.println(Alberta);
			System.out.println(BritishColumbia);
			System.out.println(Manitoba);
			System.out.println(NewBrunswick);
			System.out.println(NewfoundlandandLabrador);
			System.out.println(NorthwestTerritories);
			System.out.println(NovaScotia);
			System.out.println(Nunavut);
			System.out.println(Ontario);
			System.out.println(PrinceEdwardIsland);
			System.out.println(Quebec);
			System.out.println(Saskatchewan);
			System.out.println(Yukon);
			
		}
		
	}
	public static List<String> CountryOfOrigin()
	{
		//this code is not working 
		List<String> countryofOrigin= new ArrayList<String>();
		FileInputStream fis= null;
		try {
			fis = new FileInputStream("C://Users//NKaur//workspace//Compasstoconnect//src//test//java//resources//Drop List.xlsx");
		} catch (FileNotFoundException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		XSSFWorkbook workbook =null;
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			System.out.print("check your file path");
			e.printStackTrace();
		}
		int sheets = workbook.getNumberOfSheets();
		for(int i =0; i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("CI_CountryOfBirth"))
			{
				XSSFSheet sheet = workbook.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();				
				 while (rows.hasNext()) 
		         {
		             Row row = rows.next();
		             //For each row, iterate through all the columns
		             Iterator<Cell> cellIterator = row.cellIterator();
		             while (cellIterator.hasNext()) 
		             {
		                 Cell cell2 = cellIterator.next();
		                 if(cell2.getColumnIndex()==0)
		                 {
		                   countryofOrigin.add(cell2.getStringCellValue());
		                 }	                    
		             }
		            
		         }
			}
		}
		System.out.println(countryofOrigin);
		System.out.println(countryofOrigin.size());
		return countryofOrigin;
	}
	public static String chooseRandomCountryofOrigin(List<String> nativeCountry)
	{
		Random rand = new Random();
		return nativeCountry.get(rand.nextInt(nativeCountry.size()));
	}
}
