package listeners;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import utility.CommonUtils;



public class Listeners  implements ITestListener  {
	WebDriver driver;
	CommonUtils com = new CommonUtils();
	@Override
	public void onFinish(ITestContext arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(ITestContext arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("***** Error "+result.getName()+" test has failed *****");
    	com.Screenshot(result.getName());
    	System.out.println();
    	com.ExtentRep(result);
    	
    	System.out.println();
	}

	@Override
	public void onTestSkipped(ITestResult Result) {
		System.out.println("The name of the testcase Skipped is :"+Result.getName());
		
	}

	@Override
	public void onTestStart(ITestResult arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println("The name of the testcase passed is : "+result.getName());
		com.Screenshot(result.getName());
    	System.out.println();
    	com.ExtentRep(result);
    	System.out.println();
	}

}
