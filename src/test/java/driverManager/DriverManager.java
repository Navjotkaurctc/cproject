package driverManager;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverManager {
	
	private static WebDriver driver;
	static FileInputStream fis;
	static Properties prop;
	
	
	public static void initDriver() 
	{
		try{
			fis = new FileInputStream("C:/Users/NKaur/workspace/Compasstoconnect/src/test/java/resources/data.properties");
			prop = new Properties();
			prop.load(fis);
		}catch(FileNotFoundException e )
		{
			e.getStackTrace();
		}catch(IOException e)
		{
			e.getMessage();
		}
		
		if(prop.getProperty("browser").equalsIgnoreCase("chrome"))
		{
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		}
		else if(prop.getProperty("browser").equalsIgnoreCase("firefox"))
		{
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			
		}
		else if(prop.getProperty("browser").equalsIgnoreCase("safari"))
		{
			driver = new SafariDriver();
		}
		else if(prop.getProperty("browser").equalsIgnoreCase("opera"))
		{
			WebDriverManager.operadriver().setup();
			driver = new OperaDriver();
		}
		else if(prop.getProperty("browser").equalsIgnoreCase("ie"))
		{
			WebDriverManager.iedriver().setup();
			driver = new InternetExplorerDriver();
		}
		else
		{
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			System.out.println("since no browser was selected that is why chrome has been selected");
		}
	}
	
	public static WebDriver getDriverInstance()
	{
		if(driver==null)
		{
			initDriver();
		}
		return driver;
	}
	
	

}
