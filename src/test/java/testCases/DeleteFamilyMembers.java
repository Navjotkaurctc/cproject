package testCases;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import driverManager.DriverManager;
import junit.framework.Assert;
import pageObjects.ClientProfile;
import pageObjects.HomePage;
import pageObjects.ServicePage;
import pageObjects.ServiceSpoListPage;
import pageObjects.SigninPage;
import pageObjects.WelcomeUserPage;
import utility.CommonUtils;

@Listeners(listeners.Listeners.class)
public class DeleteFamilyMembers {
	WebDriver driver =DriverManager.getDriverInstance();
	HomePage homepage;
	SigninPage sp;
	SignUpTest signUpTest;
	WelcomeUserPage wup;
	ClientProfile clientProfile;
	ServicePage servicePage;
	ServiceSpoListPage spoList;
	static FileInputStream fis;
	static Properties prop;
	
	@Test
	public void checkCurrentUrl()
	{
		try{
			fis = new FileInputStream("C:/Users/NKaur/workspace/Compasstoconnect/src/test/java/resources/data.properties");
			prop = new Properties();
			prop.load(fis);
		}catch(FileNotFoundException e )
		{
			e.getStackTrace();
		}catch(IOException e)
		{
			e.getMessage();
		}
		CommonUtils.openApp(driver);
		homepage=new HomePage(driver);
		homepage.clicksignin();
		driver.getCurrentUrl();
		System.out.println(driver.getCurrentUrl());
		Assert.assertEquals(prop.getProperty("url")+"Login/LoginUser", driver.getCurrentUrl());
	}
	
	@Test(dependsOnMethods={"checkCurrentUrl"})//In this test case we are adding all the required values in the profile and making a request thus ensuring that the whole process is working
	public void deleteFamilyDetails() 
	{
		try{
			fis = new FileInputStream("C:/Users/NKaur/workspace/Compasstoconnect/src/test/java/resources/clientProfile.properties");
			prop = new Properties();
			prop.load(fis);
		}catch(FileNotFoundException e )
		{
			e.getStackTrace();
		}catch(IOException e)
		{
			e.getMessage();
		}
		sp = new SigninPage(driver);
		homepage = new HomePage(driver);
		sp = new SigninPage(driver);
		wup = new WelcomeUserPage(driver);
		clientProfile = new ClientProfile(driver);
		servicePage = new ServicePage(driver);
		spoList = new ServiceSpoListPage(driver);
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		//Email used for testing purposes
		String email = prop.getProperty("username");
		String password = prop.getProperty("password");
		sp.addEmail(email);
		sp.addPassword(password);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("gcs=false");
		sp.signin();
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[@id='dropdownMenuLink']"))));
		wup.clickUser();
		wup.clickProfile();
		Assert.assertTrue("Condition is satisfied", driver.getTitle().equalsIgnoreCase("Your Profile - Compass to Connect"));
		driver.findElement(By.id("profile-additional-information-tab")).click();
		//clientProfile.clickAdditionalInformationTab();
		//click on family accordion
		clientProfile.clickFamilyAccordian();
		List<WebElement> list = driver.findElements(By.xpath("//table[@id='adnlFamily']//tbody//tr"));
		Iterator<WebElement> j = list.iterator();
		int i=1;
		while(j.hasNext())
		{
				try{
					driver.findElement(By.xpath("//button[@id='idbtndel-0']")).click();
					System.out.println(i+" record has been deleted");
					driver.findElement(By.id("profile-additional-information-tab")).click();
					//clientProfile.clickAdditionalInformationTab();
					clientProfile.clickFamilyAccordian();
					i++;
				}catch(Exception ex)
				{
					ex.getStackTrace();
					System.out.println("You have successfully deleted all the additional family information");
					break;
				}
			
		}
		System.out.println("it works");
		
	}
	@AfterSuite
	public void terminate()
	{
		driver.quit();
	}
}
