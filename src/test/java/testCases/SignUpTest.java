package testCases;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import driverManager.DriverManager;
import pageObjects.ClientProfile;
import pageObjects.HomePage;
import pageObjects.RegisterPage;
import utility.CommonUtils;

public class SignUpTest {
	WebDriver driver=DriverManager.getDriverInstance();
	ClientProfile cp;
	HomePage homepage ;
	RegisterPage rp;
	FileInputStream fis;
	Properties prop;
	
	@Test(priority = 1)
	public void openAppAndClickOnRegister()
	{
		
		CommonUtils.openApp(driver);
		homepage = new HomePage(driver);
		rp=new RegisterPage(driver);
		try
		{
			homepage.clicksignup();
		}catch(NoSuchElementException ex)
		{
			System.out.println("Since profile was already logged in so further steps will log out the given profile and then logs in");
			driver.findElement(By.xpath("//div[@id='account']//a[@class=' dropdown-toggle']")).click();
			driver.findElement(By.xpath("//button[@onclick='return Logout()']")).click();
			homepage.clicksignup();
		}
		
		try{
			fis = new FileInputStream("C:/Users/NKaur/workspace/Compasstoconnect/src/test/java/resources/registerNewUser.properties");
			prop = new Properties();
			prop.load(fis);
		}catch(FileNotFoundException e )
		{
			e.getStackTrace();
		}catch(IOException e)
		{
			e.getMessage();
		}
		
	}
	
	@Test(dependsOnMethods="openAppAndClickOnRegister")
	public void addValuesToRegister()
	{
		rp.addFirstName(prop.getProperty("fname"));
		rp.addLastName(prop.getProperty("lname"));
		rp.addEmail(prop.getProperty("email"));
		rp.addPassword(prop.getProperty("pass"));
		rp.addConfirmPassword(prop.getProperty("cpass"));
		rp.clickConsent();
		try {
			Thread.sleep(5000L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rp.Accept();
		rp.clickSignUpButton();
		rp.checkMessage();
		
	}
	
	@AfterSuite
	public void terminate()
	{
		driver.quit();
	}
}
