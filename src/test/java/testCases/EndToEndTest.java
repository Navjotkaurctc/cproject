package testCases;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import driverManager.DriverManager;
import junit.framework.Assert;
import pageObjects.ClientProfile;
import pageObjects.HomePage;
import pageObjects.RequestSubmissionPage;
import pageObjects.ServicePage;
import pageObjects.ServiceSpoListPage;
import pageObjects.WelcomeUserPage;
import resources.randomDataGeneration;
import utility.CommonUtils;

@Listeners(listeners.Listeners.class)
public class EndToEndTest extends randomDataGeneration{
	WebDriver driver =DriverManager.getDriverInstance();
	HomePage homepage;
	WelcomeUserPage wup;
	ClientProfile clientProfile;
	ServicePage servicePage;
	ServiceSpoListPage spoList;
	RequestSubmissionPage reqPage;
	static FileInputStream fis;
	static Properties prop;
	
	@Test
	public void checkCurrentUrl()
	{
		try{
			fis = new FileInputStream("C:/Users/NKaur/workspace/Compasstoconnect/src/test/java/resources/data.properties");
			prop = new Properties();
			prop.load(fis);
		}catch(FileNotFoundException e )
		{
			e.getStackTrace();
		}catch(IOException e)
		{
			e.getMessage();
		}
		CommonUtils.openApp(driver);
		homepage=new HomePage(driver);
		homepage.clicksignin();
		driver.getCurrentUrl();
		System.out.println(driver.getCurrentUrl());
		Assert.assertEquals(prop.getProperty("url")+"Login/LoginUser", driver.getCurrentUrl());
		
	}
	@Test(dependsOnMethods={"checkCurrentUrl"})//In this test case we are adding all the required values in the profile and making a request thus ensuring that the whole process is working
	public void endToEndRequest() 
	{
		try{
			fis = new FileInputStream("C:/Users/NKaur/workspace/Compasstoconnect/src/test/java/resources/clientProfile.properties");
			prop = new Properties();
			prop.load(fis);
		}catch(FileNotFoundException e )
		{
			e.getStackTrace();
		}catch(IOException e)
		{
			e.getMessage();
		}
		homepage = new HomePage(driver);
		wup = new WelcomeUserPage(driver);
		clientProfile = new ClientProfile(driver);
		servicePage = new ServicePage(driver);
		spoList = new ServiceSpoListPage(driver);
		reqPage = new RequestSubmissionPage(driver);
		driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
		//Email used for testing purposes
		String email = prop.getProperty("username");
		String password = prop.getProperty("password");
		//User is signing in by using these credentials 
		CommonUtils.signIn(driver, email, password);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("gcs=false");
		WebDriverWait wait = new WebDriverWait(driver,10);
		//***************************************************************************************************
		//After this Assertion is missing that this should ensure that the control has gone to the next page
		//***************************************************************************************************
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[@id='dropdownMenuLink']"))));
		wup.clickUser();
		wup.clickProfile();
		Assert.assertTrue("Condition is satisfied", driver.getTitle().equalsIgnoreCase("Your Profile - Compass to Connect"));
		//Check if the username field is disabled as well as it should be same as the email provided by the current user 
		Assert.assertTrue(clientProfile.checkUsername().equalsIgnoreCase(email));
		clientProfile.checkUsername();
		List<String> immStatus = new ArrayList<String>();
		immStatus.add("Select immigration status");
		immStatus.add("Permanent Resident of Canada");
		immStatus.add("Protected Person");
		immStatus.add("Pending Verification (Received Letter of Approval)");
		immStatus.add("Convention Refugees");
		immStatus.add("Live-in Caregivers");
		List<String> results = new ArrayList<String>();
		//check if all the option are avaliable in the immigration category
		for(int i =0; i<clientProfile.ListOfImmigrationStatus().size();i++)
		{
			clientProfile.ListOfImmigrationStatus().get(i).getText();
			results.add(clientProfile.ListOfImmigrationStatus().get(i).getText());
			//this contains the list of immigration status that should be present in this select box and in this test case it should be validated
		}
		//System.out.println(results);
		if(immStatus.containsAll(results))
		{
			System.out.println("List matches with the list of immigration Categories [pass]");
		}
		else
		{
			System.out.println("something wrong with this script check again");
		}
		//function below is randomly selecting values from the immigration list 
		String imm=randomDataGeneration.chooseRandomImmigrationOption(immStatus);
		System.out.println(imm);
		if(imm.equals("Select immigration status"))
		{
			clientProfile.chooseImmigration("Protected Person");
			System.out.println("Since random data generation choose Select immigration status which is not acceptable as it is a required field so therfore protected person option has been selected ");
		}
		else
		{
			clientProfile.chooseImmigration(imm);
		}
		//In the above scenario it has been confirmed that the list has been validated
		//check if the uci accepts the correct value and Enter one value , random uci has been generated and it has been accepted (Numeric 8)
		clientProfile.addUCI(randomDataGeneration.uci8());
		//skipping landing and arrival date
		//Arrival date
		clientProfile.addArrivalDate(prop.getProperty("arrivalDate"));
		//Landing date
		clientProfile.addLandingDate(prop.getProperty("landingDate"));
		Actions ac = new Actions(driver);
		ac.sendKeys(Keys.TAB).build().perform();
		// add first name in the client profile
		clientProfile.addFirstName(randomDataGeneration.randomName());
		//add last name in the client profile
		clientProfile.addLastName(randomDataGeneration.randomName());
		//add date of birth for the client 
		clientProfile.addDob(prop.getProperty("dateofBirth"));
		//Phone number
		String ph = randomDataGeneration.phone();
		if(ph.length()<10 || ph.length()>10)
		{
			System.out.println("The phone that you are enetering is incorrect please try again");
		}
		else
		{
			clientProfile.addPrimaryPhoneNumber(ph);
		}
		//adding alternative phone number (It is being randomly generated every time)
		String altph = randomDataGeneration.phone();
		if(ph.equals(altph))
		{
			System.out.println("Alternative phone number cannot be same as primary phone number");
		}
		else
		{
			clientProfile.addAlternativePhoneNumber(altph);
		}
		//add value in street number and it is getting value from clientProfile.properties file
		clientProfile.addStreetNumber(prop.getProperty("street"));
		//add street name
		clientProfile.addStreetName(prop.getProperty("streetName"));
		// add unit number as it is optional value added or not it shouldn't affect the script acc to the requirement
		clientProfile.addUnitNumber(prop.getProperty("unit"));
		//select province
		clientProfile.checkProvinceOptions();
		clientProfile.selectProvince(prop.getProperty("provinceValue"));
		//select city
		clientProfile.selectCity( prop.getProperty("provinceValue"),prop.getProperty("cityValue"));
		//add postal code
		clientProfile.addPostalCode(prop.getProperty("postal"));
		//Check if the primary email address is correct 
		clientProfile.checkPrimaryEmail();
		//check if other email address field is empty or same as primary email
		clientProfile.checkOtherEmail(prop.getProperty("alt"));
		// choose country of birth/origin
		List<String> nativeCountry=randomDataGeneration.CountryOfOrigin();
		String country=randomDataGeneration.chooseRandomCountryofOrigin(nativeCountry);
		System.out.println(country);
		if(country.equals("Select your native language")||country.equals("")||country.equals("Not Selected")||country.equals("Name"))
		{
			clientProfile.chooseCountryOfOrigin("Akan");
			System.out.println("Since random data generation choose Select your native language which is not acceptable as it is a required field so therfore Akan option has been selected ");
		}
		else
		{
			clientProfile.chooseCountryOfOrigin(country);
		}
		// choose First/native language
		clientProfile.chooseNativeLanguage(prop.getProperty("nativelanguage"));
		// choose Canadian official language
		clientProfile.chooseCanadianOfficialLanguage(prop.getProperty("officialLanguage"));
		// choose gender
		clientProfile.chooseGender(prop.getProperty("gen"));
		// choose Marital status
		clientProfile.chooseMaritalStatus(prop.getProperty("mar"));
		//click on update profile and ensure that the profile is updated
		clientProfile.clickUpdateProfile();
		//update message 
		clientProfile.checkMessage();
		//click on additional tab to move further with adding more details in the profile
		clientProfile.clickAdditionalInformationTab();
		// choose highest level of education 
		clientProfile.chooseLevelOfEducation(prop.getProperty("edu"));
		// choose if you are currently enrolled 
		clientProfile.chooseCurrentEnrollment(prop.getProperty("enroll"));
		// click on update educational details
		clientProfile.submitEducationalDetails();
		//check message 
		clientProfile.checkMessage();
		//click on additional tab to move further with adding more details in the profile, this step has been added because after updating educational details the page is promted back to client profile tab
		clientProfile.clickAdditionalInformationTab();
		// click on employment in Canada tab 
		clientProfile.clickEmploymentInCanada();
		// choose source of income 
		clientProfile.chooseSourceOfIncome(prop.getProperty("source"));
		// choose employment status 
		clientProfile.chooseEmploymentStatus(prop.getProperty("empStatus"));
		// choose if you are employed in  your relevant field or not 
		clientProfile.chooseRelevantFieldOption(prop.getProperty("relevant"));
		// add original occupation and check if the options are displaying on adding three characters if yes then how many 
		clientProfile.chooseOriginalOccupation(prop.getProperty("occ"));
		// add current occupation and check if the options are displaying on adding these three characters if yes then how many 
		clientProfile.chooseCurrentOcccupation(prop.getProperty("current"));
		// add intended occupation
		clientProfile.chooseIntendedOccupation(prop.getProperty("intended"));
		//submit employment details//clickUpdateEmployment()
		clientProfile.clickUpdateEmployment();
		// as it redirects to client profile so click on additional information again
		clientProfile.clickAdditionalInformationTab();
		// click on housing accordion
		clientProfile.clickHousingAccordian();
		// choose type of your current accommodation
		clientProfile.chooseAccomodation(prop.getProperty("accomodation"));
		// update your accommodation
		clientProfile.updateHousingDetails();
		//click on family accordion
		clientProfile.clickFamilyAccordian();
		// add full name to the family member, Now click on continue search to move forward with requesting the service 
		clientProfile.addFamilyMemberDetails(prop.getProperty("FamilyMemberFullName"),prop.getProperty("relationship"), randomDataGeneration.uci10(),prop.getProperty("dobFamily"));
		// click on settlement and referrals
		servicePage.clickOnSettlmentCouncellingAndReferrals();
		//Click on an request here it is clicking on the first option
		servicePage.clickOncitizenship();
		servicePage.clickOnReviewAndProceedButton();
		servicePage.clickToSubmitRequest();
		//click on first spo to select it for requesting the service selected 
		spoList.clickOnViewAndSelectService();
		//choose the spo 
		spoList.clickOnSelectedSpo();
		// submit the request now the request has been completed
		spoList.submitRequest();
		//reqPage.Message();
		reqPage.logoutProfile();
	}
	
	@AfterSuite
	public void terminate()
	{
		driver.quit();
	}
	
}
