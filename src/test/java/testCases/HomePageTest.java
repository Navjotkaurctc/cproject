package testCases;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import driverManager.DriverManager;
import pageObjects.HomePage;
import utility.CommonUtils;

@Listeners(listeners.Listeners.class)
public class HomePageTest {
	WebDriver driver =DriverManager.getDriverInstance();
	HomePage homepage;
	FileInputStream fis;
	Properties prop;
	@Test
	public void checkCurrentUrl()
	{
		CommonUtils.openApp(driver);
		try{
			fis = new FileInputStream("C:/Users/NKaur/workspace/Compasstoconnect/src/test/java/resources/data.properties");
			prop = new Properties();
			prop.load(fis);
		}catch(FileNotFoundException e )
		{
			e.getStackTrace();
		}catch(IOException e)
		{
			e.getMessage();
		}
		homepage=new HomePage(driver);
		try
		{
			homepage.clicksignin();
		}catch(NoSuchElementException ex)
		{
			System.out.println("Since profile was already logged in so further steps will log out the given profile and then logs in");
			driver.findElement(By.xpath("//div[@id='account']//a[@class=' dropdown-toggle']")).click();
			driver.findElement(By.xpath("//button[@onclick='return Logout()']")).click();
			homepage.clicksignin();
		}
		driver.getCurrentUrl();
		//https://test.compasstoconnect.ca/Login/
		WebDriverWait wait = new WebDriverWait(driver,5);
		wait.until(ExpectedConditions.titleContains("Sign In - Compass to Connect"));
		System.out.println(driver.getCurrentUrl());
		Assert.assertTrue(driver.getCurrentUrl().equalsIgnoreCase(prop.getProperty("url")+"Login/LoginUser"));
	}
	
	@AfterSuite
	public void terminate()
	{
		driver.quit();
	}
}
