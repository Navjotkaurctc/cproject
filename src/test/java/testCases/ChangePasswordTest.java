package testCases;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import driverManager.DriverManager;
import pageObjects.HomePage;
import pageObjects.WelcomeUserPage;
import utility.CommonUtils;

public class ChangePasswordTest {
	WebDriver driver= DriverManager.getDriverInstance();
	FileInputStream fis;
	Properties prop;
	HomePage homepage;
	WelcomeUserPage wup;
	@Test
	public void ChangePasswordBy()
	{
		CommonUtils.openApp(driver);
		homepage = new HomePage(driver);
		homepage.clicksignin();
		try{
			fis = new FileInputStream("C:/Users/NKaur/workspace/Compasstoconnect/src/test/java/resources/clientProfile.properties");
			prop = new Properties();
			prop.load(fis);
		}catch(FileNotFoundException e )
		{
			e.getStackTrace();
		}catch(IOException e)
		{
			e.getMessage();
		}	
		CommonUtils.signIn(driver, prop.getProperty("username"), prop.getProperty("password"));
		wup = new WelcomeUserPage(driver);
		wup.clickUser();
		wup.clickProfile();
		wup.clickSettingsAndPreference();
		wup.clickChangePasswordAccordian();
		wup.chanePasswordWithValidations(prop.getProperty("password"), prop.getProperty("newPassword"), prop.getProperty("confirmNewPassword"));
	}
	
	@AfterSuite
	public void terminate()
	{
		driver.quit();
	}

}
