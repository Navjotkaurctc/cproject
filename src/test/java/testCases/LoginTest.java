package testCases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import dataProvider.loginDataProvider;
import driverManager.DriverManager;
import pageObjects.HomePage;
import pageObjects.SigninPage;
import utility.CommonUtils;

@Listeners(listeners.Listeners.class)
public class LoginTest {
	//This test will only work when captcha api is disable on the test server
	WebDriver driver =DriverManager.getDriverInstance();
	HomePage homePage;
	SigninPage signinpage;
	/*The test below is just opens the browser and gets the url on which testing is performed*/
	
	@BeforeMethod
	public void loginTest()
	{
		ChromeOptions op = new ChromeOptions();
		op.addArguments("-console");
		driver = new ChromeDriver();
		CommonUtils.openApp(driver);
		homePage=new HomePage(driver);
		homePage.clicksignin();	
	}
	/*the test below is created to test login functionality with different credentials meaning with different user id made on different web applications */
	@Test(dataProvider="LoginCredentials", dataProviderClass=loginDataProvider.class)
	public void userLogin(String username, String password)
	{
		signinpage = new SigninPage(driver);
		if(username.matches("^[a-zA-Z0-9_+&*-]+(?:\\."+"[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$"))
		{
			signinpage.addEmail(username);
		}
		else
		{
			System.out.println("You have entered wrong username		"+	username);
		}
		if(password.isEmpty()||password.length()<8)
		{
			System.out.println("incorrect password");
		}
		else
		{
			signinpage.addPassword(password);
		}
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("gcs=false");
		signinpage.signin();
		WebDriverWait wait = new WebDriverWait(driver,10);
		//***************************************************************************************************
		//After this Assertion is missing that this should ensure that the control has gone to the next page
		//***************************************************************************************************
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[@id='dropdownMenuLink']"))));
		Assert.assertTrue(driver.findElement(By.xpath("//a[@id='dropdownMenuLink']")).isDisplayed());
	}
	
	/*The test below terminates/quits the browser and clears memory */
	@AfterMethod
	public void terminate()
	{
		driver.quit();
	}
}
