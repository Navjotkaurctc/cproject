package utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CheckLinks {
	
	WebDriver driver;
	FileInputStream fis;
	Properties prop;
	@Test
	public void checkLinks()
	{
		try{
			fis = new FileInputStream("C:/Users/NKaur/workspace/Compasstoconnect/src/test/java/resources/data.properties");
			prop = new Properties();
			prop.load(fis);
		}catch(FileNotFoundException e )
		{
			e.getStackTrace();
		}catch(IOException e)
		{
			e.getMessage();
		}
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get(prop.getProperty("url"));
		HttpURLConnection huc = null;
		String url = "";
		int respCode = 200;
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(prop.getProperty("url"));
        List<WebElement> links = driver.findElements(By.tagName("a"));
        Iterator<WebElement> it = links.iterator();
        while(it.hasNext())
        {
            url = it.next().getAttribute("href");
            System.out.println(url);
            if(url == null || url.isEmpty()){
            	System.out.println("*********************************************");
            	System.out.println("URL is either not configured for anchor tag or it is empty");
            	System.out.println("*********************************************");
                continue;
            }
            if(!url.startsWith(prop.getProperty("url"))){
                System.out.println("URL belongs to another domain, skipping it.");
                continue;
            }
            
            try {
                huc = (HttpURLConnection)(new URL(url).openConnection());
                huc.setRequestMethod("HEAD");
                huc.connect(); 
                respCode = huc.getResponseCode();
                if(respCode >= 400){
                	System.out.println("*********************************************");
                    System.out.println(url+" is a broken link");
                    System.out.println("*********************************************");
                }
                else{
                    System.out.println(url+" is a valid link");
                }
                    
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
	}
	
	@AfterTest
	public void terminate()
	{
		driver.quit();
	}


}
