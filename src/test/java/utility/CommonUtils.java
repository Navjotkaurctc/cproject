package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.imageio.ImageIO;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import driverManager.DriverManager;
import pageObjects.CommonUtilsPageObject;
import pageObjects.SigninPage;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class CommonUtils {
	
	private static WebDriver driver=DriverManager.getDriverInstance();
	CommonUtilsPageObject com= new CommonUtilsPageObject(driver);
	String path;
	public static ExtentTest logger;
	static FileInputStream fis;
	static Properties prop;
	//method below is called only when any test case fails and its screenshot is captured under screenshots folder 
	public void Screenshot(String result)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
		Date date = new Date();  
		String currentTime = sdf.format(date);
		//File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		ru.yandex.qatools.ashot.Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
		try {
			ImageIO.write(screenshot.getImage(), "PNG", new File("C:\\Users\\NKaur\\workspace\\Compasstoconnect\\screenshots\\testFailure\\" +currentTime+result +"sc.png"));
			Path p1= Paths.get("C:\\Users\\NKaur\\workspace\\Compasstoconnect\\screenshots\\testFailure\\" +currentTime+result +"sc.png");
			path = p1.toString();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//code present under this comment is the old way of taking screen shot which was not able to scroll where the error was 
		/*try
		{
			FileUtils.copyFile(src, new File("C:\\Users\\NKaur\\workspace\\Compasstoconnect\\screenshots\\testFailure\\" +currentTime+result +"sc.png"));
		}catch(IOException e)
		{
			e.printStackTrace();
			System.out.println("Path not recognized"+ result);
		}*/
		
	}
	public void ExtentRep(ITestResult result)
	{
		ExtentHtmlReporter reporter = new ExtentHtmlReporter("./reports/extent.html");
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(reporter);
		logger = extent.createTest("Given	" +result.getName()+ "	has been passed");
		if(result.getStatus()==ITestResult.FAILURE)
    	{
    		logger.fail("Method Failed:-	"+result.getName()+"******");
    		System.out.println();
    		logger.fail(result.getInstanceName());
    		System.out.println();
    		logger.fail(result.getTestName());
    		System.out.println();
    		logger.fail("**"+result.getMethod());
    	}
    	else if(result.getStatus()==ITestResult.SUCCESS)
    	{
    		logger.pass("Method passed:-	"+result.getName()+"******");
    		System.out.println();
    		logger.pass(result.getInstanceName());
    		System.out.println();
    		logger.pass(result.getTestName());
    		System.out.println();
    		logger.pass("**"+result.getMethod());
    	}
    	else 
    	{
    		logger.skip("Method skipped:-	"+result.getName()+"******");
    		System.out.println();
    		logger.skip(result.getInstanceName());
    		System.out.println();
    		logger.skip(result.getTestName());
    		System.out.println();
    		logger.skip("**"+result.getMethod());
    	}
		try {
			logger.addScreenCaptureFromPath(path);
			logger.info("screenshot has been attached");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		extent.flush();
	}
	public static void openApp(WebDriver driver)
	{
		try{
			fis = new FileInputStream("C:/Users/NKaur/workspace/Compasstoconnect/src/test/java/resources/data.properties");
			prop = new Properties();
			prop.load(fis);
		}catch(FileNotFoundException e )
		{
			e.getStackTrace();
		}catch(IOException e)
		{
			e.getMessage();
		}
		driver.get(prop.getProperty("url"));
		driver.manage().window().maximize();
	}
	public static void signIn(WebDriver driver , String username,String pass)
	{
		
		SigninPage sp = new SigninPage(driver);
		sp.addEmail(username);
		sp.addPassword(pass);
		sp.signin();
	}
	
	
}

