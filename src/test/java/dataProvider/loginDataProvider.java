package dataProvider;

import org.testng.annotations.DataProvider;

public class loginDataProvider {

	@DataProvider(name="LoginCredentials")
	public static Object[][] loginData()
	{
		Object[][] data = new Object[5][2];
		
		// 1st row
		data[0][0] ="nkaur@tcet.com";
		data[0][1] = "12345678";
		 
		// 2nd row
		data[1][0] ="nkaurcompass@outlook.com";
		data[1][1] = "helloworld";
		 
		// 3rd row
		data[2][0] ="nkaurcompass@yahoo.com";
		data[2][1] = "12345678";
		
		// 4rth value
		data[3][0]="nkaurcompass@gmail.com";
		data[3][1]="12345678";
		
		//5th value for login
		data[4][0]="pveloso28@icloud.com";
		data[4][1]="helloworld";
		return data;
		
	}
}
